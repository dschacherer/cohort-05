#!/bin/bash
RESET="\033[0m"
RED="\033[31m"
BLINKRED="\033[31;5m"
YELLOW="\033[33m"
if [[ ! $1 ]]
then
     echo 
     echo "Usage: branch.sh <branchname>"
     echo 
else
     clear
     echo 
     echo -e "$YELLOW     Branch name entered is:"$RED "$1" $RESET
     echo 
     read -p "$(echo -e $YELLOW"     Press enter to create and checkout branch:"$RED "$1" $RESET)"
     git branch $1
     git checkout $1
     echo
     echo -e "$YELLOW     Here is the current status:" $RESET
     echo
     git status
     echo
     read -p "$(echo -e $YELLOW"     Press enter to add all changes to the branch:"$RED "$1" $RESET)"
     echo
     git add .
     echo
     echo -e "$YELLOW     Here is the current status:" $RESET
     echo
     git status
     echo
     read -p "$(echo -e $YELLOW"     Press enter to set user.email and user.name and commit branch:"$RED "$1" $RESET)"
     echo
     git config --global user.email david.schacherer@libertymutual.com
     git config --global user.name "David Schacherer"
     git commit -m "committing branch $1"
     echo
     echo -e "$YELLOW     Here is the current status:" $RESET
     echo
     git status
     echo
     read -p "$(echo -e $YELLOW"     Press enter to push branch:"$RED "$1"$YELLOW to the repository $RESET)"
     git push origin $1
     echo 
     echo -e "$BLINKRED     Pause here and go to the repository to create a pull request and merge branch $1 to master $RESET"
     echo
     read -p "$(echo -e $YELLOW"     Press enter to checkout and pull master and delete local copy of branch:"$RED "$1" $RESET)"
     echo
     git checkout master
     git pull origin master
     echo
     echo -e "$YELLOW     Here is the current status:" $RESET
     echo
     git status
     echo
fi
