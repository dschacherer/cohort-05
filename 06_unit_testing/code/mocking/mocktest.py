import os

def backup_file():
    os.rename('datafile.csv', 'datafile.csv.bak')

from unittest.mock import patch

with patch('os.rename') as rename:
        backup_file()
        rename.assert_called_with('datafile.csv', 'datafile.csv.bak')
        print('backup_file() correctly called rename!')
