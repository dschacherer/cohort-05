#!/usr/bin/env python3
import requests
import json
from datetime import datetime
SECRET_KEY = '1d8c58ed1d54f96f939e706c788650f1'

lat, long = (39.8913,-85.9198)  # McCordsville, IN

url = 'https://api.darksky.net/forecast/{key}/{lat},{long}'.format(key=SECRET_KEY, lat=lat, long=long)
response = requests.get(url)  # Defaults to a GET request
# Returns a file-like Response object, so we can read it just like File I/O
#print(response)
#print(dir(response))
forecast_data = (response.text)
#print(forecast_data)
#print(response.text)
#print(response.headers)
#print(response.status_code)
forecast = json.loads(forecast_data)
# Depending on the API, this can get really deep!  Consider breaking things down
#print(type(forecast))
time = forecast['currently']['time']
temp = forecast['currently']['temperature']
realtime = datetime.fromtimestamp(time)

#print("dt_object =", dt_object)
#print("type(dt_object) =", type(dt_object))

print(realtime, temp)

today = forecast['daily']['data'][0]

print('Today - High: {high}, Low: {low}'.format(
    high=today['temperatureHigh'], low=today['temperatureLow']))
