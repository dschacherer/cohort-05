#!/usr/bin/bash
if [[ ! $* ]]
then 
  echo "Usage:  fcheck.sh <file1> <file2> ..."
else
  for file in "$@"
  do 
    #if test -f `echo $file
  if [[ -f $file ]]
    then
      echo $file file exists
    else
      echo $file file does not exist
    fi
  done
fi
