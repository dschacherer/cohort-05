def davefunc():
    print('-' * 63)
    print('    RESTRICTED ACCESS' * 3)
    print('-' * 63)
davefunc()
print('you should not be reading this')
davefunc()

def davefunc2(message):
    davefunc()
    print(message)
    davefunc()

davefunc2('              DO NOT LOOK AT THIS SCREEN!')

def print_sum(num1, num2):
    print('the sum of', num1, 'and', num2, 'is', num1 + num2)

print_sum(32, 48)
