import random

wordlist = ('ohio', 'indiana', 'kentucky', 'wisconsin')

wordchoice = random.choice(wordlist)

gamelist = list(wordchoice)

random.shuffle(gamelist)

theword = print('The word is',''.join(gamelist))
useranswer = input('What is the word: ')

while useranswer != wordchoice:
    if useranswer == 'q':
        print('You are a quitter!')
        break
    elif (useranswer != wordchoice):
        print('Incorrect, try again!')
        useranswer = input('What is the word: ')
    else:
        break
else:
    print('You are correct!', useranswer, 'is the word')
