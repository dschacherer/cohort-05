import random

def getsecretnum():
    return random.randrange(0,9,1)

secret = ''
inc = 0
bulls = 0

while len(secret) < 4:
    num = getsecretnum()
    while str(num) not in secret:
        secret = secret + str(num)
print(secret)

while bulls != 4:
    bulls = 0
    cows = 0
    wrong = 0
    guess = input('Guess 4 numbers as nnnn: ')
    while inc < 4:
        if guess[inc] == secret[inc]:
            bulls = bulls + 1
        elif guess[inc] in secret:
            cows = cows + 1
        else:
            wrong = wrong + 1
        inc = inc +1
    print('You have', bulls,'bulls')
    print('You have', cows,'cows')
    print('You have', wrong,'wrong')

print('You guessed the number!', secret)


