import os

os.system('clear')

words = []

with open('wordlist.txt') as words_file:
    for line in words_file:
        words.append(line.strip())

vowels, consonants = [], []

for word in words:
    if word[0] in 'aeiouAEIOU':
        vowels.append(word)
    else:
        consonants.append(word)

vowels.sort()
consonants.sort()

print()
print('Vowels: ', vowels)
print()
print('Consonants: ', consonants)
