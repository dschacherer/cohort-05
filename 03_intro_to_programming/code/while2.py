import random
value = random.randint(1, 100)
guess = int(input ("Guess a number between 1 and 100: "))
print(guess)
while guess != value:
    while guess != value and guess < value:
        guess = int(input ("Your guess is too low, try again: "))
    while guess != value and guess > value:
        guess = int(input ("Your guess is too high, try again: ")) 
print("You got it, the number is: ", value)
