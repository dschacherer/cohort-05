cups = { 'tall': 12, 'grande': 16 }
print(type(cups), cups)

print('A tall cup contains', cups['tall'], 'ounces')
print('A grande cpu contains', cups['grande'], 'ounces')

if 'grande' in cups:
    print(cups['grande'])

for thing in cups:
    print(thing, cups[thing])

for v in cups.values():
    print(v)

for key, value in cups.items():
    print(key, '->', value)

print(cups.items())
