import os
import time
import random
os.system('clear')
name = input("What is your name? ")
print("Hello",name,"...Time to play hangman!")
time.sleep(1)

print("Start guessing...")
time.sleep(0.5)
#word = "secret"
guesses = ''
turns=10

with open("../wordlist.txt") as wordfile:
    words = wordfile.read().split()
word = random.choice(words)

while turns > 0:
    failed = 0
    for char in word:
        if char in guesses:
            print(char)
        else:
            print("_")
            failed +=1
    print()
    if failed == 0:
        print("You Won")
        print("The word is:",word)
        break

    guess = input("guess a character: ")
    guesses += guess
    print("Used letter list: ", sorted(guesses))
    print()
    if guess not in word:
        turns -= 1
        print("Wrong")
        print("You have",turns, "more guesses")
        if turns == 0:
            print("You Lose")
            print("The word was:",word)
