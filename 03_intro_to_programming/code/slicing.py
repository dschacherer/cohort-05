string = 'Frank Benedict eats jam in the morning'
print(string[6:9] + string[20:23] + string[24:27] + string[:5] +'lin')

alphabet = 'abcdefghijklmnopqrstuvwxyz'
print('13th letter of the alphabet is', alphabet[12])
print('Every other letter in the 1st half of the alphabet:',alphabet[:13:2])
print('Every other letter in the 2nd half of the alphabet:',alphabet[13::2])
print('The alphabet backwards is', alphabet[::-1])

#string2 = input('Enter a string: ')
#print('The last 3 characters of the string are:',string2[-3:])

string3 = 'Hello'
for i in string3[0:-1]:
   print(i,'+',end=' ')
print(string3[-1])

list1 = ['one','two','three','four']
list2 = ['1','2','3','4','5']
print(list1)
print(list1[-2:],end=' ')
print(list1[:2])
print(list2)
length= int(len(list2))
middle = int(len(list2) / 2)
print(list2[middle-1])
print(length)
