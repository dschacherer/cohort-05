import random
import os
os.system('clear')
value = random.randint(1, 100)
#print(value)
guess = int(input ("Please guess a number between 1 and 100: "))
while guess != value:
    if guess == 0:
        print("Sorry you gave up, loser")
        break
    elif guess > value:
        print("Your guess of",guess,"is too high")
    elif guess < value:
        print("Your guess of",guess,"is too low")
        print()
    guess = int(input ("Please try again, or 0 to quit trying: "))
else:
    print("You got it, the number is: ", value)
