roman_to_arabic = {
        'M': 1000,
        'D': 500,
        'C': 100,
        'L': 50,
        'X': 10,
        'V': 5,
        'I': 1
}

addthemup = 0
for key, value in roman_to_arabic.items(): 
    addthemup = addthemup + value

print(addthemup)

while True:
    romannum = input('Enter a Roman numeral: ')
    if len(romannum) == 1:
        print('roman numeral',romannum, '=',roman_to_arabic[romannum])

    if len(romannum) == 2:
        roman1 = romannum[0]
        roman2 = romannum[1]
        num1 = roman_to_arabic[roman1]
        num2 = roman_to_arabic[roman2]
        if (num1 < num2):
            print('Roman numeral',romannum, '=',num2 - num1)
        else:
            print('Roman numeral',romannum, '=',num2 + num1)

    if len(romannum) == 3:
        roman1 = romannum[0]
        roman2 = romannum[1]
        roman3 = romannum[2]
        num1 = roman_to_arabic[roman1]
        num2 = roman_to_arabic[roman2]
        num3 = roman_to_arabic[roman3]
        if (num2 < num3):
            print('Roman numeral',romannum, '=',num1 + (num3 - num2))
        else:
            print('Roman numeral',romannum, '=',num1 + num3 + num2)
