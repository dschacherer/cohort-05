FROM python:3
COPY . .
RUN apt update
RUN apt install -y python3-pip
RUN python3 -m pip install requests
CMD["read"]
ENTRYPOINT ["python", "wpapitest.py"]

