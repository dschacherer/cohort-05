import requests
import sys

POSITION_STACK_KEY = "ca30f2a7d8132e388e4b7b1177dec61d"
#POSITION_STACK_KEY = "testingfornon200returncode"

def check_http_response(some_response):
    try:
        some_response.raise_for_status()
    except:
        if some_response.status_code != 200:
            print('\n',"Website Error: ", url, response,'\n')
            sys.exit()
        else:
            print('\n',"Website access worked",'\n')

def get_location1(longitude, latitude):
    """Returns the City and Region based on the longitude and latitude provided"""


    url = "http://api.positionstack.com/v1/reverse"

    coords = longitude + "," + latitude

    payload = {
        "access_key": POSITION_STACK_KEY,
        "query": coords,
        "output": 'json'
    }

    response = requests.get(
        url, params=payload)

    check_http_response(response)

    #check_http_response()

    #try:
    #    response.raise_for_status()
    #except:
    #    if response.status_code != 200:
    #        print('\n',"Website Error: ", url, response,'\n')
    #        sys.exit()

    body_info = response.json()

    city = body_info["data"][0]["locality"]
    state = body_info["data"][0]["region_code"]


    return city, state

city, state = get_location1("41.854264", "-87.619241")
print('\n',city, state,'\n')
