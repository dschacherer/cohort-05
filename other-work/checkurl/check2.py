import requests
import sys

#POSITION_STACK_KEY = "ca30f2a7d8132e388e4b7b1177dec61d"
POSITION_STACK_KEY = "bad0f2a7d8132e388e4b7b1177dec61d"

def get_location1(longitude, latitude):
    """Returns the City and Region based on the longitude and latitude provided"""


    url = "http://api.positionstack.com/v1/reverse"

    coords = longitude + "," + latitude

    payload = {
        "access_key": POSITION_STACK_KEY,
        "query": coords,
        "output": 'json'
    }

    response = requests.get(
        url, params=payload)
    try:
        response.raise_for_status()
    except:
        if response.status_code != 200:
            print ("Website Error: ", url, response)
        #else:
        #    print ("Website Good: ", url, response)

    body_info = response.json()

    city = body_info["data"][0]["locality"]
    state = body_info["data"][0]["region_code"]


    return city, state

city, state = get_location1("41.854264", "-87.619241")
print(city, state)

#response = requests.get(url,timeout=1)

#try:
#    response.raise_for_status()
#except:
#    pass
#if response.status_code != 200:
#    print ("Website Error: ", url, response)
#else:
#    print ("Website Good: ", url, response)
