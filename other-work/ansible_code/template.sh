#!/usr/bin/bash
jinja2 tmnt.j2 -D name=donatello -D nickname=donnie -D color=purple -D primary_weapon=staff && \
jinja2 tmnt.j2 -D name=leonardo -D nickname=leo -D color=blue -D primary_weapon=sword && \
jinja2 tmnt.j2 -D name=raphael -D nickname=raph -D color=red -D primary_weapon=sai && \
jinja2 tmnt.j2 -D name=michaelangelo -D nickname=mikey -D color=orange -D primary_weapon=nunchucks
