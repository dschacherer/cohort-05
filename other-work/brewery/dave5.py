#!/bin/bash
import json
import requests
import os
zipapi_key = 'YDEacao3pvUtVti8xt8j0pycye1qUbktVqFjFSBdG4Ox7bnUbsMnKz8ENV8Klnyl'

def getzipcode():
    zipapi_url = "http://www.zipcodeapi.com/rest/%s/city-zips.json/%s/%s" % (zipapi_key, city, state)
    response = requests.get(zipapi_url)
    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return {"zip_codes":[]}

def findbrewerybyzip():
    api_url = 'https://api.openbrewerydb.org/breweries?by_postal='+ (zip)
    response = requests.get(api_url)
    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None
def printfunction():
    if len(brewery) is not 0:
        for dict in range(len(brewery)):
            bdict = brewery[(dict)]
            print()
            print("\t\t\t\t",bdict['name'])
            print("\t\t\t\t",bdict['street'])
            print("\t\t\t\t",bdict['city'], bdict['state'], bdict['postal_code'])
            print("\t\t\t\t",bdict['website_url'])
            print()
    else:
        print()
        print('\t\t\t\tDid not find any breweries in zip code',zip,'!')

os.system('clear')

while True:
    print()
    print('\033[1m \033[94m \033[4m' +  "\t\t\tSHACK'S BREWERY FINDER\n" + '\033[0m')
    print("\t\t\tEnter a 5 digit zipcode OR a city with 2 letter state code (or q to exit)")
    zip = input("\t\t\tExample - 46055 OR Austin, TX): ")

    if zip.isdecimal():
        if len(zip) == 5:
            brewery = findbrewerybyzip()
            printfunction()
        else:
            print()
            print("\033[91m\t\t\tError: The zipcode must be 5 digits\033[0m")
    else:
        citystate = [x.strip() for x in zip.split(",")]
        if len(citystate) == 2: 
            city = citystate[0]
            state = citystate[1]
            zipcodes = getzipcode()
            if not zipcodes["zip_codes"]:
                print(zipcodes)
                print("\033[91m\t\t\tNo zip code found for",city,",",state,"\033[0m")
            else:
                for key, values in zipcodes.items():
                    if(isinstance(values, list)):
                        for value in values:
                            zip = value
                            brewery = findbrewerybyzip()
                            printfunction()
        elif citystate[0] == "q":
            print()
            print("\033[92m\t\t\tThanks for using SHACK's BREWERY FINDER - Have a great day!'\033[0m")
            print()
            print()
            exit(0)
        else:
            print()
            print("\033[91m\t\t\tError: City and State format is not correct, please see example Jodi\033[0m")
